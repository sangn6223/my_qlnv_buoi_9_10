function renderDSNV(dsNv) {
  let contentHTML = "";
  for (i = dsNv.length - 1; i >= 0; i--) {
    let item = dsNv[i];
    let contentTr = `
        <tr>
        <td>${item.iD}</td>
        <td>${item.nameNv}</td>
        <td>${item.email}</td>
        <td>${0}</td>
        <td>
        <button class="btn btn-danger" id="deleteSv" onclick="xoaSV(${
          item.iD
        })">Xóa</button>
        <button class="btn btn-success" id="SuaSv" onclick="SuaSv(${
          item.iD
        })">Sửa</button>
        </td>
        <td></td>
        </tr>
        `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
